var mysql = require("mysql");

var connections=[];

    function setConnection(settings){
        var connection=mysql.createConnection(settings);
        connection.connect(function(error){
            try{
                if(error) throw error;
                console.log("Base de datos: Conectada y funcional");
                connection.on('error',function(error){
                    console.log("Base de datos: Error, Reconectando...");
                    if((error.code === 'PROTOCOL_CONNECTION_LOST' || error.code === 'ECONNRESET')){
                        setConnection(settings);
                    }
                });
                connections[settings.database]=connection;
            } catch (error){
                console.log("Base de datos: Error, Intentando conectar...");
                console.log(error);
                setConnection(settings);
            }
        })
    }
/*Username: aht5caD95U Password: t97J8FjeDY Database Name: aht5caD95U Server: remotemysql.com Port: 3306 */
setConnection({
    host: "remotemysql.com",
    user: "aht5caD95U",
    password: "t97J8FjeDY",
    port: "3306",
    database: "aht5caD95U"
});

module.exports = connections;